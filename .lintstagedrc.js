// From next.js' lint-staged
// https://github.com/zeit/next.js/blob/canary/lint-staged.config.js

const escape = require('shell-quote').quote;
const isWin = process.platform === 'win32';

module.exports = {
  '**/*.{js,jsx,ts,tsx,json,css,scss,html,yml,yaml}': filenames => {
    const escapedFileNames = filenames
      .map(filename => `"${isWin ? filename : escape([filename])}"`)
      .join(' ');
    return [`prettier --write ${escapedFileNames}`, `git add ${escapedFileNames}`];
  },
};
